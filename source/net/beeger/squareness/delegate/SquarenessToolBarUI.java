/*
  Copyright (c) 2003-2006, Robert F. Beeger (robert at beeger dot net)
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

      * Redistributions of source code must retain the above copyright notice, this list
        of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright notice, this
        list of conditions and the following disclaimer in the documentation and/or other
        materials provided with the distribution.
      * Neither the name of 'Robert F. Beeger' nor the names of his contributors may be
        used to endorse or promote products derived from this software without specific
        prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Visit http://squareness.beeger.net/ for new releases of Squareness Look And Feel and other
  skins from the Squareness series.
*/
package net.beeger.squareness.delegate;

import java.awt.Component;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicToolBarUI;

import net.beeger.squareness.util.SquarenessBorderFactory;

/**
 * The Squareness Tool Bar UI delegate.
 */
public class SquarenessToolBarUI extends BasicToolBarUI
{
  /**
   * Create the ui delegate for the given component
   * 
   * @param component The component for which to create the ui delegate
   * @return The created ui delegate
   */
  public static ComponentUI createUI (JComponent component)
  {
    return new SquarenessToolBarUI();
  }

  /**
   * Install the UI delegate for the given component.
   *
   * @param component The component for which to install the UI delegate.
   */
  public void installUI (JComponent component)
  {
    super.installUI(component);
    toolBar.addContainerListener(new ContainerListener()
    {
      public void componentAdded (ContainerEvent e)
      {
        JComponent child = (JComponent) e.getChild();
      }

      public void componentRemoved (ContainerEvent e)
      {
      }
    });
  }

  /**
   * Create a rollover border. This border will be used if rollover borders are enabled.
   */
  protected Border createRolloverBorder ()
  {
    return SquarenessBorderFactory.getButtonRolloverBorder();
  }

  /**
   * Create a non rollover border. This border will be used if rollover borders are disabled.
   */
  protected Border createNonRolloverBorder ()
  {
    return SquarenessBorderFactory.getButtonNonRolloverBorder();
  }

  /**
   * Set the non rollover border on the given component.
   *
   * @param component The component to set the non rollover border on.
   */
  protected void setBorderToNonRollover (Component component)
  {
    super.setBorderToNonRollover(component);
    if (component instanceof AbstractButton)
    {
      AbstractButton button = (AbstractButton) component;
      if (button.getBorder() instanceof UIResource)
      {
        if (button instanceof JToggleButton && !(button instanceof JCheckBox))
        {
          // only install this border for the ToggleButton
          button.setBorder(SquarenessBorderFactory.getButtonNonRolloverBorder());
        }
      }
    }
  }
}