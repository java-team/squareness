/*
  Copyright (c) 2003-2006, Robert F. Beeger (robert at beeger dot net)
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

      * Redistributions of source code must retain the above copyright notice, this list
        of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright notice, this
        list of conditions and the following disclaimer in the documentation and/or other
        materials provided with the distribution.
      * Neither the name of 'Robert F. Beeger' nor the names of his contributors may be
        used to endorse or promote products derived from this software without specific
        prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Visit http://squareness.beeger.net/ for new releases of Squareness Look And Feel and other
  skins from the Squareness series.
*/
package net.beeger.squareness.delegate;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.JSlider;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSliderUI;

import net.beeger.squareness.SquarenessLookAndFeel;

/**
 * The Squareness Slider UI delegate.
 */
public class SquarenessSliderUI extends BasicSliderUI
{
  /**
   * Create an UI delegate for the given component.
   *
   * @param component The component for which to create the UI delegate.
   * @return The created UI delegate.
   */
  public static ComponentUI createUI (JComponent component)
  {
    return new SquarenessSliderUI((JSlider) component);
  }

  /**
   * Create the UI delegate.
   *
   * @param slider The slider for which this delegate will be used.
   */
  public SquarenessSliderUI (JSlider slider)
  {
    super(slider);
  }

  /**
   * Paint the track of the slider.
   *
   * @param graphics The graphics object to use for painting the track.
   */
  public void paintTrack (Graphics graphics)
  {
    Color oldColor = graphics.getColor();
    int x, y, width, height;

    if (slider.getOrientation() == JSlider.HORIZONTAL)
    {
      x = trackRect.x;
      y = trackRect.y + (trackRect.height >> 1) - 2;
      width = trackRect.width;
      height = 5;
    }
    else
    {
      x = trackRect.x + (trackRect.width >> 1) - 2;
      y = trackRect.y;
      width = 5;
      height = trackRect.height;
    }

    graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getProgressBarBackgroundColor());
    graphics.fillRect(x, y, width, height);

    graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getNormalBorderColor());
    graphics.drawRect(x, y, width - 1, height - 1);

    if (slider.getClientProperty("JSlider.isFilled") != null
        && ((Boolean) slider.getClientProperty("JSlider.isFilled")).booleanValue())
    {
      graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getSelectedControlBackgroundColor());

      if (slider.getOrientation() == JSlider.HORIZONTAL)
      {
        if (drawInverted())
        {
          graphics.fillRect(thumbRect.x, y + 1, width - 2 - (thumbRect.x - x), height - 2);
        }
        else
        {
          graphics.fillRect(x + 1, y + 1, thumbRect.x - x, height - 2);
        }
      }
      else
      {
        if (drawInverted())
        {
          graphics.fillRect(x + 1, thumbRect.y, width - 2 - (thumbRect.y - y), height - 2);
        }
        else
        {
          graphics.fillRect(x + 1, y + 1, width - 2, thumbRect.y - y);
        }
      }
    }

    graphics.setColor(oldColor);
  }

  /**
   * Paint the thumb of the slider.
   *
   * @param graphics The graphics object to use for the painting.
   */
  public void paintThumb (Graphics graphics)
  {
    Color oldColor = graphics.getColor();

    graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getNormalControlBackgroundColor());
    graphics.fillRect(thumbRect.x, thumbRect.y, thumbRect.width, thumbRect.height);
    if (slider.isEnabled())
    {
      graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getNormalBorderColor());
    }
    else
    {
      graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getDisabledBorderColor());
    }
    graphics.drawRect(thumbRect.x, thumbRect.y, thumbRect.width - 1, thumbRect.height - 1);

    if (slider.getPaintTicks())
    {
      int yBot = thumbRect.y + thumbRect.height - 1;
      int xRight = thumbRect.x + thumbRect.width - 1;
      if (slider.getOrientation() == JSlider.HORIZONTAL)
      {
        graphics.drawLine(thumbRect.x + 2, yBot - 5, thumbRect.x + 5, yBot - 2);
        graphics.drawLine(thumbRect.x + 3, yBot - 5, thumbRect.x + 5, yBot - 3);
        graphics.drawLine(thumbRect.x + 3, yBot - 6, thumbRect.x + 5, yBot - 4);

        graphics.drawLine(xRight - 2, yBot - 5, thumbRect.x + 5, yBot - 2);
        graphics.drawLine(xRight - 3, yBot - 5, thumbRect.x + 5, yBot - 3);
        graphics.drawLine(xRight - 3, yBot - 6, thumbRect.x + 5, yBot - 4);
      }
      else
      {
        graphics.drawLine(xRight - 5, thumbRect.y + 2, xRight - 2, thumbRect.y + 5);
        graphics.drawLine(xRight - 5, thumbRect.y + 3, xRight - 3, thumbRect.y + 5);
        graphics.drawLine(xRight - 6, thumbRect.y + 3, xRight - 4, thumbRect.y + 5);

        graphics.drawLine(xRight - 5, yBot - 2, xRight - 2, thumbRect.y + 5);
        graphics.drawLine(xRight - 5, yBot - 3, xRight - 3, thumbRect.y + 5);
        graphics.drawLine(xRight - 6, yBot - 3, xRight - 4, thumbRect.y + 5);
      }
    }

    graphics.setColor(oldColor);
  }

  /**
   * Paint a minor tick for a horizontal slider.
   *
   * @param graphics The graphics object to use for painting.
   * @param tickBounds The bounds of the tick.
   * @param x The x position at which to paint the tick.
   */
  protected void paintMinorTickForHorizSlider (Graphics graphics, Rectangle tickBounds, int x)
  {
    graphics.drawLine(x, 1, x, (tickBounds.height >> 1) - 1);
  }

  /**
   * Paint a major tick for a horizontal slider.
   *
   * @param graphics The graphics object to use for painting.
   * @param tickBounds The bounds of the tick.
   * @param x The x position at which to paint the tick.
   */
  protected void paintMajorTickForHorizSlider (Graphics graphics, Rectangle tickBounds, int x)
  {
    graphics.drawLine(x, 1, x, tickBounds.height - 2);
  }

  /**
   * Paint a minor tick for a vertical slider.
   *
   * @param graphics The graphics object to use for painting.
   * @param tickBounds The bounds of the tick.
   * @param y The y position at which to paint the tick.
   */
  protected void paintMinorTickForVertSlider (Graphics graphics, Rectangle tickBounds, int y)
  {
    graphics.drawLine(1, y, (tickBounds.width >> 1) - 1, y);
  }

  /**
   * Paint a major tick for a vertical slider.
   *
   * @param graphics The graphics object to use for painting.
   * @param tickBounds The bounds of the tick.
   * @param y The y position at which to paint the tick.
   */
  protected void paintMajorTickForVertSlider (Graphics graphics, Rectangle tickBounds, int y)
  {
    graphics.drawLine(1, y, tickBounds.width - 2, y);
  }

}