/*
  Copyright (c) 2003-2006, Robert F. Beeger (robert at beeger dot net)
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

      * Redistributions of source code must retain the above copyright notice, this list
        of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright notice, this
        list of conditions and the following disclaimer in the documentation and/or other
        materials provided with the distribution.
      * Neither the name of 'Robert F. Beeger' nor the names of his contributors may be
        used to endorse or promote products derived from this software without specific
        prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Visit http://squareness.beeger.net/ for new releases of Squareness Look And Feel and other
  skins from the Squareness series.
*/
package net.beeger.squareness.delegate;

import java.awt.Graphics;
import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicButtonUI;

import net.beeger.squareness.SquarenessConstants;
import net.beeger.squareness.util.SquarenessBorderFactory;
import net.beeger.squareness.util.SquarenessButtonPainter;
import net.beeger.squareness.util.SquarenessListenerFactory;

/**
 * The Squareness Button UI delegate.
 */
public class SquarenessButtonUI extends BasicButtonUI
{
  /**
   * Create the UI delegate for the given component
   * 
   * @param component The component for which to create the ui delegate
   * @return The created ui delegate
   */
  public static ComponentUI createUI (JComponent component)
  {
    return _buttonUI;
  }

  /**
   * Install the UI delegate for the given component
   * 
   * @param component The component for which to install the ui delegate.
   */
  public void installUI (JComponent component)
  {
    component.putClientProperty(SquarenessConstants.ROLLOVER_CLENT_PROPERTY_KEY, Boolean.FALSE);
    component.addMouseListener(SquarenessListenerFactory.getButtonRolloverMouseListener());
    super.installUI(component);
  }

  /**
   * Uninstall the UI delegate
   *
   * @param component The component from which to uninstall the UI delegate.
   */
  public void uninstallUI (JComponent component)
  {
    component.removeMouseListener(SquarenessListenerFactory.getButtonRolloverMouseListener());
    super.uninstallUI(component);
  }

  /**
   * Paint the component.
   * 
   * @param graphics  The graphics resource used to paint the component
   * @param component The component to paint.
   */
  public void paint (Graphics graphics, JComponent component)
  {
    if (!"yes".equals(component.getClientProperty(SquarenessConstants.CHECKED_FOR_JEDIT_ROLLOVER)))
    {
      component.putClientProperty(SquarenessConstants.CHECKED_FOR_JEDIT_ROLLOVER, "yes");
      boolean jEditRolloverButton = false;
      Class currentClass = component.getClass();
      String currentClassName = currentClass.getName();
      while (!jEditRolloverButton && currentClass != null
             && !(currentClassName.startsWith("java.") || currentClassName.startsWith("javax.")))
      {
        if ("org.gjt.sp.jedit.gui.RolloverButton".equals(currentClass.getName()))
        {
          jEditRolloverButton = true;
        }
        else
        {
          currentClass = currentClass.getSuperclass();
        }
      }
      if (jEditRolloverButton)
      {
        component.setBorder(SquarenessBorderFactory.getButtonRolloverBorder());
      }
    }

    SquarenessButtonPainter.paintButton(graphics, (AbstractButton) component);
    super.paint(graphics, component);
  }

  /**
   * One instance handles all buttons.
   */
  private static SquarenessButtonUI _buttonUI = new SquarenessButtonUI();
}