/*
  Copyright (c) 2003-2006, Robert F. Beeger (robert at beeger dot net)
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

      * Redistributions of source code must retain the above copyright notice, this list
        of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright notice, this
        list of conditions and the following disclaimer in the documentation and/or other
        materials provided with the distribution.
      * Neither the name of 'Robert F. Beeger' nor the names of his contributors may be
        used to endorse or promote products derived from this software without specific
        prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Visit http://squareness.beeger.net/ for new releases of Squareness Look And Feel and other
  skins from the Squareness series.
*/
package net.beeger.squareness;

/**
 * <p>This class contains all the constants used in this Look And Feel.</p>
 */
public final class SquarenessConstants
{
  //
  // public interface
  //

  public static final String SCROLLBAR_THUMB_PRESSED_CLIENT_PROPERTY_KEY = "SquarenessScrollBarThumbPressed";
  public static final String SCROLLBAR_TRACK_PRESSED_CLIENT_PROPERTY_KEY = "SquarenessScrollBarTrackPressed";

  public static final String SCROLL_ARROW_LEFT_KEY = "scrollArrowLeft";
  public static final String SCROLL_ARROW_RIGHT_KEY = "scrollArrowRight";
  public static final String SCROLL_ARROW_UP_KEY = "scrollArrowUp";
  public static final String SCROLL_ARROW_DOWN_KEY = "scrollArrowDown";
  public static final String SPIN_UP_KEY = "spinUp";
  public static final String SPIN_DOWN_KEY = "spinDown";

  public static final String ROLLOVER_CLENT_PROPERTY_KEY = "SquarenessRollover";
  public static final String CHECKED_FOR_JEDIT_ROLLOVER ="SquarenessCheckedJEDITROLLOVER";

  private SquarenessConstants ()
  {
  }
}