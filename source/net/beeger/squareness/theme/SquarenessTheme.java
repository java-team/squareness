/*
  Copyright (c) 2003-2006, Robert F. Beeger (robert at beeger dot net)
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

      * Redistributions of source code must retain the above copyright notice, this list
        of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright notice, this
        list of conditions and the following disclaimer in the documentation and/or other
        materials provided with the distribution.
      * Neither the name of 'Robert F. Beeger' nor the names of his contributors may be
        used to endorse or promote products derived from this software without specific
        prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Visit http://squareness.beeger.net/ for new releases of Squareness Look And Feel and other
  skins from the Squareness series.
*/
package net.beeger.squareness.theme;

import java.awt.Font;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.metal.MetalTheme;

/**
 * The common abstract superclass for all Squareness theme classes.
 */
public abstract class SquarenessTheme extends MetalTheme
{
  public FontUIResource getControlTextFont ()
  {
    if (_controlTextFont == null)
    {
      _controlTextFont = new FontUIResource("Dialog", Font.PLAIN, 11);
    }
    return _controlTextFont;
  }

  public FontUIResource getMenuTextFont ()
  {
    if (_menuTextFont == null)
    {
      _menuTextFont = new FontUIResource("Dialog", Font.PLAIN, 11);
    }
    return _menuTextFont;
  }

  public FontUIResource getSubTextFont ()
  {
    if (_subTextFont == null)
    {
      _subTextFont = new FontUIResource("Dialog", Font.PLAIN, 9);
    }
    return _subTextFont;
  }

  public FontUIResource getSystemTextFont ()
  {
    if (_systemTextFont == null)
    {
      _systemTextFont = new FontUIResource("Dialog", Font.PLAIN, 11);
    }
    return _systemTextFont;
  }

  public FontUIResource getUserTextFont ()
  {
    if (_userTextFont == null)
    {
      _userTextFont = new FontUIResource("Dialog", Font.PLAIN, 11);
    }
    return _userTextFont;
  }

  public FontUIResource getWindowTitleFont ()
  {
    if (_windowTitleFont == null)
    {
      _windowTitleFont = new FontUIResource("Dialog", Font.BOLD, 11);
    }
    return _windowTitleFont;
  }

  /**
   * Since some applications explicitely use those metal colors, we define them to matching colors
   * in the Squareness color space. Still some applications may look funny since the Squareness colors have other
   * meanings and usages than the Metal colors and cannot be mapped totally correctly.
   * @return The Squarness normal border color
   */
  protected ColorUIResource getPrimary1 ()
  {
    return getNormalBorderColor();
  }

  /**
   * Since some applications explicitely use those metal colors, we define them to matching colors
   * in the Squareness color space. Still some applications may look funny since the Squareness colors have other
   * meanings and usages than the Metal colors and cannot be mapped totally correctly.
   * @return The Squarness selected control background color
   */
  protected ColorUIResource getPrimary2 ()
  {
    return getSelectedControlBackgroundColor();
  }

  /**
   * Since some applications explicitely use those metal colors, we define them to matching colors
   * in the Squareness color space. Still some applications may look funny since the Squareness colors have other
   * meanings and usages than the Metal colors and cannot be mapped totally correctly.
   * @return The Squarness selected control background color
   */
  protected ColorUIResource getPrimary3 ()
  {
    return getSelectedControlBackgroundColor();
  }

  /**
   * Since some applications explicitely use those metal colors, we define them to matching colors
   * in the Squareness color space. Still some applications may look funny since the Squareness colors have other
   * meanings and usages than the Metal colors and cannot be mapped totally correctly.
   * @return The Squarness normal border color
   */
  protected ColorUIResource getSecondary1 ()
  {
    return getNormalBorderColor();
  }

  /**
   * Since some applications explicitely use those metal colors, we define them to matching colors
   * in the Squareness color space. Still some applications may look funny since the Squareness colors have other
   * meanings and usages than the Metal colors and cannot be mapped totally correctly.
   * @return The Squarness disabled border color
   */
  protected ColorUIResource getSecondary2 ()
  {
    return getDisabledBorderColor();
  }

  /**
   * Since some applications explicitely use those metal colors, we define them to matching colors
   * in the Squareness color space. Still some applications may look funny since the Squareness colors have other
   * meanings and usages than the Metal colors and cannot be mapped totally correctly.
   * @return The Squarness window background color
   */
  protected ColorUIResource getSecondary3 ()
  {
    return getWindowBackgroundColor();
  }

  /**
   * <p>Return the desktop color.</p>
   * <p>This color is used as the background color of desktop panes.</p>
   * @return the desktop color
   */
  public abstract ColorUIResource getDesktopColor();

  /**
   * <p>Return the window background color.</p>
   * <p>This color is used as the background color of windows of all sorts.</p>
   * @return the window background color
   */
  public abstract ColorUIResource getWindowBackgroundColor();

  /**
   * <p>Return the inactive window border color.</p>
   * <p>This color is used for the border of inactive frames.</p>
   * @return the inactive window border color
   */
  public abstract ColorUIResource getInactiveWindowBorderColor();

  /**
   * <p>Return the normal border color.</p>
   * <p>This color is used for borders of all sorts: control borders, window, menu etc. borders.</p>
   * @return the normal border color
   */
  public abstract ColorUIResource getNormalBorderColor();

  /**
   * <p>Return the disabled border color.</p>
   * <p>This color is used for borders of all sorts where the component to which the border is applied is disabled.</p>
   * @return the disabled border color
   */
  public abstract ColorUIResource getDisabledBorderColor();

  /**
   * <p>Return the default button border color.</p>
   * <p>This color is used for the border of default buttons.</p>
   * @return the default button border color
   */
  public abstract ColorUIResource getDefaultButtonBorderColor();

  /**
   * <p>Return the normal control background color.</p>
   * <p>This color is used for the background of normal unselected and not disabled controls
   * such as buttons and scroll bar thumbs</p>
   * @return the normal control background color
   */
  public abstract ColorUIResource getNormalControlBackgroundColor();

  /**
   * <p>Return the selected control background color.</p>
   * <p>This color is used for the background of selected controls and controls over which the mouse pointer hovers.</p>
   * @return the selected control background color
   */
  public abstract ColorUIResource getSelectedControlBackgroundColor();

  /**
   * <p>Return the selected control background shadow color.</p>
   * <p>This color is used for the shadow in the background of selected controls.</p>
   * @return the selected control background shadow color
   */
  public abstract ColorUIResource getSelectedControlBackgroundShadowColor();

  /**
   * <p>Return the progress bar background color.</p>
   * <p>This color is used for the background of progress bars and for the tracks of scroll bars.</p>
   * @return the progress bar background color
   */
  public abstract ColorUIResource getProgressBarBackgroundColor();

  /**
   * <p>Return the pressed scroll bar track background color.</p>
   * <p>This color is used for the background of scroll bar tracks when the track is pressed (i.e. you clicked the left
   * mouse button while the mouse pointer was over the track have not released it yet.).</p>
   * @return the pressed scroll bar track background color
   */
  public abstract ColorUIResource getPressedScrollBarTrackBackgroundColor();

  /**
   * <p>Return the text input background color.</p>
   * <p>This color is used for the background of controls where text can be typed in (text fields, text areas etc.)</p>
   * @return the text input background color
   */
  public abstract ColorUIResource getTextInputBackgroundColor();

  /**
   * <p>Return the text color.</p>
   * <p>All text - no matter where it is printed on - is printed in this color.)</p> 
   * @return the text color
   */
  public abstract ColorUIResource getTextColor();

  /**
   * Is this theme a dark theme.<br>
   * The information whether a theme is dark can be useful for component plugin writers
   * needing to come up with new colors that fit into the theme. 
   * @return true if this theme is a dark theme, false otherwise
   */
  public abstract boolean isDark();

  private FontUIResource _controlTextFont;
  private FontUIResource _menuTextFont;
  private FontUIResource _subTextFont;
  private FontUIResource _systemTextFont;
  private FontUIResource _userTextFont;
  private FontUIResource _windowTitleFont;
}