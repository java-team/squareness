/*
  Copyright (c) 2003-2006, Robert F. Beeger (robert at beeger dot net)
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

      * Redistributions of source code must retain the above copyright notice, this list
        of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright notice, this
        list of conditions and the following disclaimer in the documentation and/or other
        materials provided with the distribution.
      * Neither the name of 'Robert F. Beeger' nor the names of his contributors may be
        used to endorse or promote products derived from this software without specific
        prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Visit http://squareness.beeger.net/ for new releases of Squareness Look And Feel and other
  skins from the Squareness series.
*/
package net.beeger.squareness.theme;

import javax.swing.plaf.ColorUIResource;

/**
 * Created by IntelliJ IDEA.
 * User: Robert F. Beeger
 * Date: 22.08.2004
 * Time: 20:12:55
 * To change this template use File | Settings | File Templates.
 */
public class DefaultSquarenessTheme extends SquarenessTheme
{
  public String getName ()
  {
    return "Original";
  }

  public ColorUIResource getDesktopColor ()
  {
    return new ColorUIResource(234, 216, 164);
  }

  public ColorUIResource getWindowBackgroundColor ()
  {
    return new ColorUIResource(247,242,225);    
  }

  public ColorUIResource getDisabledBorderColor ()
  {
    return new ColorUIResource(134, 144, 150);
  }

  public ColorUIResource getNormalBorderColor ()
  {
    return new ColorUIResource(34, 57, 73);
  }

  public ColorUIResource getSelectedControlBackgroundColor ()
  {
    return new ColorUIResource(211, 211, 42);
  }



  public ColorUIResource getInactiveWindowBorderColor ()
  {
    return new ColorUIResource(184,175,119);
  }

  public ColorUIResource getDefaultButtonBorderColor ()
  {
    return new ColorUIResource(170,59,34);
  }

  public ColorUIResource getNormalControlBackgroundColor ()
  {
    return new ColorUIResource(219,196,99);
  }

  public ColorUIResource getSelectedControlBackgroundShadowColor ()
  {
    return new ColorUIResource(163,159,28);
  }

  public ColorUIResource getProgressBarBackgroundColor ()
  {
    return new ColorUIResource(245,246,220);
  }

  public ColorUIResource getPressedScrollBarTrackBackgroundColor ()
  {
    return new ColorUIResource(237,225,185);
  }

  public ColorUIResource getTextInputBackgroundColor ()
  {
    return new ColorUIResource(255,255,255);
  }

  public ColorUIResource getTextColor ()
  {
    return new ColorUIResource(0,0,0);
  }

  public boolean isDark ()
  {
    return false;
  }
}